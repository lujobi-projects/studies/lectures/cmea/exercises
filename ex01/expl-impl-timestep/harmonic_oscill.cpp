#include <Eigen/Core>
#include <Eigen/LU> 
#include <vector>
#include <iostream>
#include "writer.hpp"
#include <assert.h>

/// Uses the explicit Euler method to compute y from time 0 to time T 
/// where y is a 2x1 vector solving the linear system of ODEs as in the exercise
///
/// @param[out] yT at the end of the call, this will have vNext
/// @param[in] y0 the initial conditions
/// @param[in] zeta the damping factor (see exercise)
/// @param[in] dt the step size
/// @param[in] T the final time at which to compute the solution.
///
/// The first component of y (the position) will be stored to y1, the second component (the velocity) to y2. The i-th entry of y1 (resp. y2) will contain the first (resp. second) component of y at time i*dt. 
///

//----------------explicitEulerBegin----------------
void explicitEuler(std::vector<double> & y1, std::vector<double> &y2, std::vector<double> & time,
	const Eigen::Vector2d& y0,
	    double zeta, double dt, double T) {

    y1.clear();
    y2.clear();
    time.clear();

    Eigen::Vector2d y_n;

    // initialize all vectors and the initial cond.
    y_n << y0[0], y0[1];

    // ODE in Matrix form
    Eigen::Matrix2d A;
    A << 0,1,-1,-2*zeta;

    for(double t = 0; t <= T; t += dt){
        // calc new y_n
        y_n = y_n + dt*(A*y_n);

        y1.push_back(y_n[0]); // push_back might not be the most efficient concept here, maybe allocate the vector beforehand
        y2.push_back(y_n[1]);
        time.push_back(t);
    }
}
//----------------explicitEulerEnd----------------

// Implements the implicit Euler. Analogous to explicit Euler, same input and output parameters
//----------------implicitEulerBegin----------------
void implicitEuler(std::vector<double> & y1, std::vector<double> & y2, std::vector<double> & time,
	const Eigen::Vector2d& y0,
		   double zeta, double dt, double T) {

    y1.clear();
    y2.clear();
    time.clear();

    Eigen::Vector2d y_n;

    // initialize all vectors and the initial cond.
    y_n << y0[0], y0[1];

    // ODE in Matrix form
    Eigen::Matrix2d A;
    A << 0,1,-1,-2*zeta;

    for(double t = 0; t <= T; t += dt){
        // calc new y_n
        Eigen::Matrix2d lhs = Eigen::Matrix2d::Identity() - dt*A;
        y_n = lhs.lu().solve((const Eigen::Vector2d) y_n);

        // push_back might not be the most efficient concept here, maybe allocate the vector beforehand
        y1.push_back(y_n[0]);
        y2.push_back(y_n[1]);
        time.push_back(t);
    }
}
//----------------implicitEulerEnd----------------


//----------------energyBegin----------------
// Energy computation given the velocity. Assume the energy vector to be already initialized with the correct size.
void Energy(const std::vector<double> & v, std::vector<double> & energy) {
    assert(v.size() == energy.size());
    // Mapping std::vector to Eigen::Vector
    Eigen::VectorXd eigen_vec_v = Eigen::Map<const Eigen::VectorXd, Eigen::Unaligned>(v.data(), v.size());
    // Using elementwise multiplication and map to the energy vector
    Eigen::VectorXd::Map(&energy[0], eigen_vec_v.size()) = 0.5 * eigen_vec_v.array() * eigen_vec_v.array();
}
//----------------energyEnd----------------

int main() {
	
	double T = 20.0;
	double dt = 0.1; // Change this for explicit / implicit time stepping comparison
	const Eigen::Vector2d y0(1,0);
	double zeta=0.2;
	std::vector<double> y1;
	std::vector<double> y2;
	std::vector<double> time;
	explicitEuler(y1,y2,time,y0,zeta,dt,T);
	writeToFile("position_expl.txt", y1);
	writeToFile("velocity_expl.txt",y2);
	writeToFile("time_expl.txt",time);
	std::vector<double> energy(y2.size());
	Energy(y2,energy);
	writeToFile("energy_expl.txt",energy);
	y1.assign(y1.size(),0);
	y2.assign(y2.size(),0);
	time.assign(time.size(),0);
	energy.assign(energy.size(),0);
	implicitEuler(y1,y2,time,y0,zeta,dt,T);
	writeToFile("position_impl.txt", y1);
	writeToFile("velocity_impl.txt",y2);
	writeToFile("time_impl.txt",time);
	Energy(y2,energy);
	writeToFile("energy_impl.txt",energy);
	
	return 0;
}
