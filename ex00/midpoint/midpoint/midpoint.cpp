#include "midpoint.hpp"

double midpoint_rule(double a, double b, int n, FunctionPointer f) {
    double step = (b - a)/n;
    double integral = 0;

    for (int i = 0; i < n; i++){
        integral += f(a + (i + .5) * step);
    }

    return step*integral;
}
