#include <iostream>
#include <Eigen/Dense>


int main(int argc, char **argv){

    // Declare Eigen vector type for doubles
    using vector_t =  Eigen::VectorXd ;

    // Initialize Eigen vector containing body weight in Kg(X)
    vector_t X(15);
    X << 2 , 2.2 , 2.4 , 2.2 , 2.6 , 2.2 , 2.4 , 2.4 , 2.5 , 2.7 , 2.6 , 2.2, 2.5 , 2.5 , 2.5 ;

    // Initialize Eigen vector containing heart weight in g (Y)
    vector_t Y(15);
    Y << 6.5 , 7.2 , 7.3 , 7.6 , 7.7 , 7.9 , 7.9 , 7.9 , 7.9 , 8.0 , 8.3, 8.5, 8.6 , 8.8 , 8.8;

    // TODO: Initialize Eigen Matrix A
    vector_t aux(15);
    aux.setOnes();

    Eigen::MatrixXd A(15,2);
    A << aux, X;

    // Create LHS = A'*A
    Eigen::MatrixXd LHS = A.transpose()*A;

    // TODO: Create RHS = A'*Y
    vector_t RHS = A.transpose()*Y;

    // TODO: Solve system and output coefficients b_0 and b_1
    vector_t x = LHS.lu().solve(RHS);

    std::cout << x;

  return 0;
}
