#include "midpoint.hpp" // To use our library
#include "writer.hpp" // This is the output function to write to file

// We store our results in a vector
#include <vector>

// On some platforms we need to add this in order
// to get M_PI defined
#define _USE_MATH_DEFINES

// for our usual math functions and constants
#include <math.h>

double f(double x) {
    return sin(M_PI * x);
}


int main(int, char**) {
    double a = 0.2;
    double b = 1.3;
    double exact = -cos(M_PI*a)/M_PI + cos(M_PI*b)/M_PI;

    int n = 16;

    std::vector<double> res;

    for(int i = 4; i < 12; i++){
        res.push_back(abs(exact - midpoint_rule(a, b, n, f)));
        n *= 2;
    }

    writeToFile("series0_1_d_errors.txt", res);

    return 0;
}
